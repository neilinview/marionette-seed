# marionette-seed — seed for backbone, marionette, gulp, sass, bootstrap, font-awesome, require etc.

This project is an application skeleton for a typical backbone marionette web app.
You can use it to quickly bootstrap your backbone marionette webapp projects and dev environment for these
projects.

The seed contains a sample marionette application and is preconfigured to install the Marionette
framework.

The seed app doesn't do much, just lists some todos.


## Getting Started

To get you started you can simply clone the marionette-seed repository and install the dependencies:

### Prerequisites

You need git to clone the marionette-seed repository.

You must have node.js and its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

Once you have cloned the app, install the dependencies:

### Install Dependencies

There are npm and bower dependencies

```
sudo npm install
sudo bower install

```

You should find that you have two new
folders in your project.

* `node_modules`
* `bower_components`

### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
sudo npm start

```

Now browse to the app at `http://localhost:8000/index.html`.
define(["app", "apps/todos/list/list_view"], function(App, View) {
  "use strict";

  /**
  * @author Neil Huyton
  * @module ChannelsApp.ChannelsApp/List
  */

  App.module("TodosApp.List", function(List, App, Backbone, Marionette, $, _){
    
    /**
    * @name ChannelsApp/List/Controller
    */
    List.Controller = {
      
      /**
      * @name module:ChannelsApp.ChannelsApp/List.listChannels
      * @function
      */
      listTodos: function(){

        require(["entities/todo"], function(){
          var fetchingTodos = App.request("todo:entities");
          var todosListLayout = new View.Layout();

          $.when(fetchingTodos).done(function(todos){
            var todosListView = new View.Todos({
              collection: todos
            });
          
            todosListLayout.on("show", function(){
              todosListLayout.todosRegion.show(todosListView);
            });

            todosListView.on("childview:todo:edit", function(childView, args){
              App.trigger("todo:edit", args.model.get("id"));
            });

            App.mainRegion.show(todosListLayout);
            
          });

        });

      }
    }
  });

});
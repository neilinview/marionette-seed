define(["app",
        "tpl!apps/todos/list/templates/layout.html",
        "tpl!apps/todos/list/templates/none.html",
        "tpl!apps/todos/list/templates/list.html",
        "tpl!apps/todos/list/templates/list_item.html"], 
        function(App, layoutTpl, noneTpl, listTpl, listItemTpl) {
  "use strict";

  /**
  * @module TodosApp.TodosApp/List/View
  * @description The views for TodosApp.List
  * @author Neil Huyton
  */

  App.module("TodosApp.List.View", function(View, App, Backbone, Marionette, $, _){
    View.Layout = Marionette.LayoutView.extend(/** @lends module:TodosApp.TodosApp/List/View/Layout.prototype */{
        /** 
        *  @class TodosApp/List/View/Layout
        *  @description A {@link http://marionettejs.com/docs/v2.3.1/marionette.layoutview.html|Marionette.LayoutView} for TodosApp.List.
        *  <p>Use in conjunction with template
        *  <strong>apps/todos/list/templates/layout.html</strong></p>
        *  @augments Marionette.LayoutView
        *  @constructs View.Layout object
        *  @param {TodoCollection} collection
        */

        template: layoutTpl,

        regions: {
          todosRegion: ".layout-todo-list"
        }
    });

    View.Todo = Marionette.ItemView.extend(/** @lends module:TodosApp.TodosApp/List/View/Todo.prototype */{
      /** 
      *  @class TodosApp/List/View/Todo
      *  @description A {@link http://marionettejs.com/docs/v2.3.1/marionette.itemview.html|Marionette.ItemView} for TodosApp.List.
      *  <p>Use in conjunction with template
      *  <strong>apps/todos/list/templates/list_item.html</strong></p>
      *  @augments Marionette.ItemView
      *  @constructs View.Todo object
      */

      tagName: "tr",
      template: listItemTpl,
    });

    View.Empty = Marionette.ItemView.extend(/** @lends module:TodosApp.TodosApp/List/View/Empty.prototype */{
      /** 
      *  @class TodosApp/List/View/Empty
      *  @description A {@link http://marionettejs.com/docs/v2.3.1/marionette.itemview.html|Marionette.ItemView} for TodosApp.List.
      *  <p>Use in conjunction with template
      *  <strong>apps/todos/list/templates/none.html</strong></p>
      *  @augments Marionette.ItemView
      *  @constructs View.Empty object
      */
      template: "#todo-list-none",
      tagName: "tr",
      className: "alert"
    });

    View.Todos = Marionette.CompositeView.extend(/** @lends module:TodosApp.TodosApp/List/View/Todos.prototype */{
      /** 
      *  @class TodosApp/List/View/Todos
      *  @description A {@link http://marionettejs.com/docs/v2.3.1/compositeview.itemview.html|Marionette.CompositeView} for TodosApp.List.
      *  <p>Use in conjunction with template
      *  <strong>apps/todos/list/templates/list.html</strong></p>
      *  @augments Marionette.CompositeView
      *  @constructs View.Todos object
      */
      tagName: "table",
      className: "table todo_list",
      template: listTpl,
      emptyView: View.Empty,
      childView: View.Todo,
      childViewContainer: "tbody",
    });

  });
  
  return App.TodosApp.List.View;
});
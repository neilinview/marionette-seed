define(["app"], function(App) {
  "use strict";

  /**
  * @author Neil Huyton
  * @description The parent application for todos
  * @module TodosApp
  */

  App.module("TodosApp", function(TodosApp, App, Backbone, Marionette, $, _){

    TodosApp.Router = Marionette.AppRouter.extend(/** @lends module:TodosApp.TodosApp/Router.prototype */{
      /** 
      * @class TodosApp/Router
      * @description A {@link http://backbonejs.org/#Model|Backbone.Router} for TodosApp.
      * <p>appRoutes:</p>
      * <p>"todos"      : "listTodos"</p>
      * @augments Backbone.Router
      * @constructs TodosApp.Router object
      */
      appRoutes: {
        "todos"      : "listTodos",
      }
    });

    /**
    * @name TodosApp/API
    * @description The API for the TodosApp application
    */
    var API = {
      /**
      * @name TodosApp/API/listTodos
      * @description Shows the todo list
      * @function
      */
      listTodos: function(){
        require(["apps/todos/list/list_controller"], function(ListController) {
          TodosApp.List.Controller.listTodos();
        });
      },
    };

    App.on("todos:list", function(){
      Backbone.navigate("todos");
      API.listTodos();
    });

    App.addInitializer(function(){
      new TodosApp.Router({
        controller: API
      });
    });
  });

});
define(["app"], function(App) {
  "use strict";

  /**
  * @author Neil Huyton
  * @description Holds all the models and collections for the app
  * @module Entities
  */

  App.module("Entities", function(Entities, App, Backbone, Marionette, $, _){
    
    Entities.TodoModel = Backbone.Model.extend(/** @lends module:Entities.Entities/TodoModel.prototype */{
      /** @class module:Entities.Entities/TodoModel
      *  @description A {@link http://backbonejs.org/#Model|Backbone.Model} for a Todo.<p>urlRoot - '/todos'</p>
      *  @augments Backbone.Model
      *  @constructs TodoModel object
      *  @param {array} attributes - date
      *  @param {array} options
      */
      //urlRoot: "http://localhost:7001/epg/todos?auth=0xD33CAB42162911E497B3A89046B1ACB0",
      urlRoot: "json_data/todos.json",

      defaults: {
        id: "",
        name: "",
      },
    });

    Entities.TodoCollection = Backbone.Collection.extend(/** @lends module:Entities.Entities/TodoCollection.prototype */{
      /** @class module:Entities.Entities/TodoCollection
      *  @description A {@link http://backbonejs.org/#Collection|Backbone.Collection} of todos
      *  @augments Backbone.Collection
      *  @constructs TodoCollection object
      *  @param {TodoModel} models - array  of TodoModel
      *  @param {array} options
      */
      //url: "http://localhost:7001/epg/todos/?auth=0xD33CAB42162911E497B3A89046B1ACB0",
      url: "json_data/todos.json",
      model: Entities.TodoModel
    });

    /**
    * @name Entities/API
    * @description The API for Entities.TodoModel module
    */
    var API = {
      /**
      * @name module:Entities.Entities/TodoModel.getTodoEntities
      * @function
      * @returns {Array}
      */
      getTodoEntities: function(){
        var todos = new Entities.TodoCollection();
        var defer = $.Deferred();
        todos.fetch({
          success: function(data){
            defer.resolve(data);
          }
        });
        var promise = defer.promise();
        $.when(promise).done(function(todos){
        });
        return promise;
      },

      /**
      * @name module:Entities.Entities/TodoModel.getTodoEntity
      * @function
      * @param {String} todo_id
      * @returns {Todo}
      */
      getTodoEntity: function(id){
        var todo = new Entities.TodoModel({id: id});
        var defer = $.Deferred();
        setTimeout(function(){
          todo.fetch({
            success: function(data){
              defer.resolve(data);
            },
            error: function(data){
              defer.resolve(undefined);
            }
          });
        }, 2000);
        return defer.promise();
      }
    };

    App.reqres.setHandler("todo:entities", function(){
      return API.getTodoEntities();
    });

    App.reqres.setHandler("todo:entity", function(id){
      return API.getTodoEntity(id);
    });
  });

});
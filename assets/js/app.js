define(["marionette"], function(Marionette) {
    
    /**
    * @namespace App
    * @global
    */
    var App = new Marionette.Application();

    App.addRegions({
      mainRegion: ".layout-main",
    });

    Backbone.navigate = function(route,  options){
      options || (options = {});
      Backbone.history.navigate(route, options);
    };

    Backbone.getCurrentRoute = function(){
      return Backbone.history.fragment;
    };

    App.on("start", function(){
        require([
          "assets/js/apps/todos/todos_app.js",
          ], function(){

            if(Backbone.history){
              Backbone.history.start();

              if(Backbone.getCurrentRoute() === ""){
                App.trigger("todos:list");
              }
            }
            
        });
    });
    return App;
});
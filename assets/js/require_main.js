requirejs.config({
    baseUrl: "assets/js",
    config: {
        moment: {
            noGlobal: true
        }
    },
    paths: {
        backbone        : "vendor/backbone/backbone",
        jquery          : "vendor/jquery/jquery",
        marionette      : "vendor/marionette/backbone.marionette",
        text            : "vendor/requirejs-text/text",
    	tpl             : "vendor/requirejs-underscore-tpl/underscore-tpl",
        underscore      : "vendor/underscore/underscore",
        bootstrap       : "vendor/bootstrap",
    },
    shim: {
        underscore: {
            exports: "_"
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        marionette: {
            deps: ['backbone'],
            exports: 'Marionette'
        },
    	tpl: ["text"],
        bootstrap : { 
            deps :[ 'jquery' ] 
        },
    }
});

require(["app"], function(App){
    App.start();
});
var gulp = require('gulp'),     
    sass = require('gulp-sass') 
    notify = require("gulp-notify") 
    bower = require('gulp-bower');

var config = {
     fontDir: './assets/fonts',
    cssDir: './assets/css',
    sassDir: './assets/sass',
    vendorDir: './assets/js/vendor' ,
     bowerDir: './bower_components' 
}

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(config.bowerDir)) 
});

gulp.task('icons', function() { 
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest(config.fontDir)); 
});

gulp.task('jquery', function() { 
    return gulp.src(config.bowerDir + '/jquery/dist/jquery.js') 
        .pipe(gulp.dest(config.vendorDir + '/jquery')); 
});

gulp.task('requirejs', function() { 
    return gulp.src(config.bowerDir + '/requirejs/require.js') 
        .pipe(gulp.dest(config.vendorDir + '/requirejs')); 
});

gulp.task('backbone', function() { 
    return gulp.src(config.bowerDir + '/backbone/backbone.js') 
        .pipe(gulp.dest(config.vendorDir + '/backbone')); 
});

gulp.task('marionette', function() { 
    return gulp.src(config.bowerDir + '/marionette/lib/backbone.marionette.js') 
        .pipe(gulp.dest(config.vendorDir + '/marionette/')); 
});

gulp.task('bootstrapjs', function() { 
    return gulp.src(config.bowerDir + '/bootstrap-sass-official/assets/javascripts/bootstrap.js') 
        .pipe(gulp.dest(config.vendorDir + '/bootstrap/')); 
});

gulp.task('underscore', function() { 
    return gulp.src(config.bowerDir + '/underscore/underscore.js') 
        .pipe(gulp.dest(config.vendorDir + '/underscore/')); 
});

gulp.task('requirejs-underscore-tpl', function() { 
    return gulp.src(config.bowerDir + '/requirejs-underscore-tpl/underscore-tpl.js') 
        .pipe(gulp.dest(config.vendorDir + '/requirejs-underscore-tpl/')); 
});

gulp.task('requirejs-text', function() { 
    return gulp.src(config.bowerDir + '/requirejs-text/text.js') 
        .pipe(gulp.dest(config.vendorDir + '/requirejs-text/')); 
});

gulp.task('sass', function () {
	gulp.src(config.sassDir + '/app.scss')
	.pipe(sass({
		outputStyle: 'compressed',
		includePaths: [
		 	'./resources/sass',
		     config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
		    	config.bowerDir + '/fontawesome/scss',
         ]
	}))
	.pipe(gulp.dest(config.cssDir));
});

// Rerun the task when a file changes
 gulp.task('watch', function() {
     gulp.watch(config.sassDir + '/**/*.scss', ['sass']); 
});

  gulp.task('default', [
    'bower', 
    'icons', 
    'jquery', 
    'requirejs', 
    'backbone', 
    'marionette', 
    'bootstrapjs', 
    'underscore', 
    'requirejs-underscore-tpl', 
    'requirejs-text', 
    'sass'
    ]
);